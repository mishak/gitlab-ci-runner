Changelog
=========

1.2.0
-----

Main:

- Fix #2: multiple tags are now correctly handled
- Fix #3: ruby\_block resource is always run
- Fix #6: Make repo URL and gpgkey an attribute
- Fix #7: Support multiple env strings
- Add ChefSpec matchers for LWRP

Tests:

- Switch to kitchen-docker\_cli instead of kitchen-docker
- Clean kitchen.yml, use instance names & add skip\_preparation
- Add tags in gitlab-ci tests
- Use Continuous Integration with gitlab-ci
- Can specify retries in package, set 1 in tests

1.1.0
-----

Main:

- Fix cookbook to work with latest available version of gitlab (8.5) and
  gitlab-runner (1.0.4)
- Be completely generic and accept any runner options
  + rename token to registration\_token to match runner cli option name

Tests:

- Change kitchen driver to docker\_cli
- Use official docker image for gitlab
- Remove useless monkey patching

Misc:

- Fix all rubocop offenses
- Add license, refactor README and update it
- Add default attribute 'runners' with a small documentation

1.0.0
-----

- Initial version with Centos 7 support
